# Trello Board Management - Project Snapshot!
##### The below shows how we used trello board for project management activities.
##### We created several labels to be used in cards for quick searches, had criteria to add date, assignee to each card.
##### We used color for distinction.

## Project (Dark Blue)
<a href="https://git.chalmers.se/-/ide/project/courses/dit355/test-teams-formation/team-10/documentation/tree/main/-/Snapshot/project1.jpeg" target=”_blank” ><img src="Snapshot/project1.jpeg/"/></a>

<a href="https://git.chalmers.se/-/ide/project/courses/dit355/test-teams-formation/team-10/documentation/tree/main/-/Snapshot/project2.jpeg" target=”_blank” ><img src="Snapshot/project2.jpeg/"/></a>

## Requirement (Light Blue)
##### We created 3 different lanes for requirement handling. A requirement card shall be added to the "new" lane by PO or any member in sync with PO. The writer uses the template card to write the requirement. In the upcoming meeting, new requirements get picked up for discussion. After thorough discussion and analysis, the team makes a decision whether it shall be accepted or rejected and moved to specific lane. Sometimes an action point is created if it requires further investigation before making a decision.
<a href="https://git.chalmers.se/-/ide/project/courses/dit355/test-teams-formation/team-10/documentation/tree/main/-/Snapshot/req1.jpeg" target=”_blank” ><img src="Snapshot/req1.jpeg/"/></a>

## Product Backlog
##### Initially the requirements were to be handled as SRS however after few weeks into the project, we received information that requirements should be handled as user stories so we created a product backlog. The product backlog is divided into several areas. Many times user story resulted in related tasks so a "task" lane is created for better management.
<a href="https://git.chalmers.se/-/ide/project/courses/dit355/test-teams-formation/team-10/documentation/tree/main/-/Snapshot/us1.jpeg" target=”_blank” ><img src="Snapshot/us1.jpeg/"/></a>

## Sprint 1 / Milestone 1 (Orange)
##### For better management of sprints, we created 4 lanes which are backlog, ongoing, to be reviewed, done. During sprint planning meeting, sprint scope is decided and respective cards were put into sprint backlog lane. Once the assigned member start working on the assigned card, it will be moved to ongoing lane. When the code is ready and a merge request is created in the gitlab, it is moved to be reviewed lane. On completion of the review and accepted by the team, the card is moved to the done lane.
<a href="https://git.chalmers.se/-/ide/project/courses/dit355/test-teams-formation/team-10/documentation/tree/main/-/Snapshot/sprint1.jpeg" target=”_blank” ><img src="Snapshot/sprint1.jpeg/"/></a>

## Sprint 2 / Milestone 2 (Yellow)
<a href="https://git.chalmers.se/-/ide/project/courses/dit355/test-teams-formation/team-10/documentation/tree/main/-/Snapshot/sprint2.jpeg" target=”_blank” ><img src="Snapshot/sprint2.jpeg/"/></a>

## Sprint 3 / Milestone 3 (Purple)
<a href="https://git.chalmers.se/-/ide/project/courses/dit355/test-teams-formation/team-10/documentation/tree/main/-/Snapshot/sprint3.jpeg" target=”_blank” ><img src="Snapshot/sprint3.jpeg/"/></a>

## Sprint 4 / Milestone 4 (Pink) - Draft
<a href="https://git.chalmers.se/-/ide/project/courses/dit355/test-teams-formation/team-10/documentation/tree/main/-/Snapshot/sprint4.jpeg" target=”_blank” ><img src="Snapshot/sprint4.jpeg/"/></a>

## Sprint 4 / Milestone 4 (Pink) - In Use
<a href="https://git.chalmers.se/-/ide/project/courses/dit355/test-teams-formation/team-10/documentation/tree/main/-/Snapshot/sprint4jan7.jpeg" target=”_blank” ><img src="Snapshot/sprint4jan7.jpeg/"/></a>

## User stories/tasks spilling over several sprints
##### Several labels were created in trello board. If a user story/task is spread over several sprints, we added labels for better management.
<a href="https://git.chalmers.se/-/ide/project/courses/dit355/test-teams-formation/team-10/documentation/tree/main/-/Snapshot/cardspreadoversprints.jpeg" target=”_blank” ><img src="Snapshot/cardspreadoversprints.jpeg/"/></a>

[Go Back](https://git.chalmers.se/courses/dit355/test-teams-formation/team-10/documentation)
