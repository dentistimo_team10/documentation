# Sequence Diagram

<a href="https://git.chalmers.se/courses/dit355/test-teams-formation/team-10/documentation/-/raw/main/Diagrams/V2/Sequence_EnterWebsite_V2.jpeg" target=”_blank” ><img src="Diagrams/V2/Sequence_EnterWebsite_V2.jpeg" width="800" height="600"/></a> 

<a href="https://git.chalmers.se/courses/dit355/test-teams-formation/team-10/documentation/-/raw/main/Diagrams/V2/Sequence_ViewAvailability_V2.jpeg" target=”_blank” ><img src="Diagrams/V2/Sequence_ViewAvailability_V2.jpeg" width="800" height="600"/></a>  

<a href="https://git.chalmers.se/courses/dit355/test-teams-formation/team-10/documentation/-/raw/main/Diagrams/V2/Sequence_BookAppointment_V2.jpeg" target=”_blank” ><img src="Diagrams/V2/Sequence_BookAppointment_V2.jpeg" width="800" height="600"/></a>  

<a href="https://git.chalmers.se/courses/dit355/test-teams-formation/team-10/documentation/-/raw/main/Diagrams/V2/Sequence_Filter_V2.jpeg" target=”_blank” ><img src="Diagrams/V2/Sequence_Filter_V2.jpeg" width="800" height="500"/></a>

[Go Back](https://git.chalmers.se/courses/dit355/test-teams-formation/team-10/documentation)
