# Project Velocity (Burndown Chart)
##### Initially the requirements were to be handled as SRS however after few weeks into the project, we received information that requirements should be handled as user stories and we also need to provide a velocity chart. That is why the first few weeks below is not included.

<a href="https://git.chalmers.se/courses/dit355/test-teams-formation/team-10/documentation/-/raw/main/Diagrams/V4/Velocity_V4.jpeg" target=”_blank” ><img src="Diagrams/V4/Velocity_V4.jpeg"/></a>

[Go Back](https://git.chalmers.se/courses/dit355/test-teams-formation/team-10/documentation)
