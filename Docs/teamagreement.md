# Team Agreement

The team shall abide by the following rules:
- Each member will listen and respect other members’ opinions and be respectful when voicing their own opinion.
- Each member will do their best to be on time to meetings.
- Each member will let the team know in advance if they cannot attend a meeting or will be late.
- Each member will stay up to date on the project (through GitLab, Trello, Discord).
- Each member will be transparent when communicating (i.e. discussions on campus should be repeated on discord, if they were related to the project). 


### Task procurement procedure

During our sprint planning, we will divide up the tasks in the backlog according to the skills and availability of the team. If these tasks are completed before scheduled, then the member may assign oneself to another task on the Trello board, otherwise they may offer help to other team members.

### GitLab procedure

When adding something to the repository, members will create a new branch with a reasonable name. Once all their changes are committed, a merge request will be submitted and their changes will be reviewed by another team member. If approved, the branch may be merged into the main. If denied, the requested changes should be addressed and reviewed once more in order to be eligible for merging. 

### Schedule

1. There will be no project work during weekends, but members can work on their tasks if they want to. A member who is working cannot demand other members to work/reply/respond/help during these days.

2. There is an expectation that team members will not be able to be contacted, attend team meetings, or commit work (though they may choose to do so at their own discretion), during the period that they reserve for the holidays.

#### Holidays

| Team member | Holidays |
| ------ | ------ |
| Lucas | Dec 22nd, 23rd, 24th, 30th, 31st  |
| Anna |  Dec 23rd, 24th, 30th, & 31st |
| Ediz |  Dec 22th, 23th, 24th, 30th, 31st |
| Maryam |  Dec 27th - 31st |
| Himank |  Dec 24th, 31st |
| Adam |  Dec 22nd, 23rd, 24th, 30th, 31st |
| Hannah |  Dec 22nd, 23rd, 24th, 30th, 31st |   

#### Agreement signed by: 

_Sicily Brannen  
Lucas Erik Emanuel(Lee) Nordgren  
Hannah Shiels  
Himank Meattle    
Ediz Genc  
Maryam Esmaeili Darestani   
Adam Magnus_


[Go Back](https://git.chalmers.se/courses/dit355/test-teams-formation/team-10/documentation)
